//codes	
$content = preg_replace ( '/\{\{\{([^\n]+?)\}\}\}/i', '`$1`', $content );
//title
$content = preg_replace ( '/\=\=\=\=\s?(.+?)\s?\=\=\=\=/i', '### $1', $content );
$content = preg_replace ( '/\=\=\=\s?(.+?)\s?\=\=\=/i', '## $1', $content );
$content = preg_replace ( '/\=\=\s?(.+?)\s?\=\=/i', '# $1', $content );
$content = preg_replace ( '/\=\s?(.+?)\s?\=[\s\n]*/i', '', $content );
//imgs
$content = preg_replace ( '/\[(http[^\s\[\]]+)\s([^\[\]]+)\]/i', '[$2]($1)', $content );
$content = preg_replace ( '/\!(([A-Z][a-z0-9]+){2,})/i', '$1', $content );
$content = preg_replace ( "/'''(.+)'''/i", '*$1*', $content );
$content = preg_replace ( "/''(.+)''/i", '_$1_', $content );
//list
$content = preg_replace ( '/\[\[([^\n]+?)\]\]/i', '$1', $content );
$content = preg_replace ( '/\*/i', '* ', $content );
$content = preg_replace ( '/^\s\d\./i', '1.', $content );